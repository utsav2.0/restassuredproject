package Utility_Common_Methods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Extractor {

	public static ArrayList<String> Excel_data_reader(String filename, String sheetname, String tc_name)
			throws IOException {
		ArrayList<String> Arraydata = new ArrayList<String>();
		String proj_dir = System.getProperty("user.dir");
		System.out.println(proj_dir);

		// Step 1 Create the object of FIle input Stream to locate the data file (read
		// file name)
		FileInputStream FIS = new FileInputStream(proj_dir + "\\Data_Files\\" + filename + ".xlsx");

		// Step 2 Create the XSSFWorkbook object to open the excel file
		XSSFWorkbook WB = new XSSFWorkbook(FIS);

		// Step 3 fetch the number of sheets available in the excel file
		int count = WB.getNumberOfSheets();

		// Step 4 access the sheet as per the input sheet name

		for (int i = 0; i < count; i++) {
			String Sheetname = WB.getSheetName(i);
			if (Sheetname.equals(sheetname)) {
				System.out.println(Sheetname);

				// reading the mathcing sheet
				XSSFSheet sheet = WB.getSheetAt(i);
				Iterator<Row> row = sheet.iterator(); //iterating rows
			//	row.next();
				while (row.hasNext()) {
					Row datarow = row.next();
					String tcname = datarow.getCell(0).getStringCellValue();
					// System.out.println(tcname);
					if (tcname.equals(tc_name)) {
						Iterator<Cell> cellvalues = datarow.iterator(); //iterating cells
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							// System.out.println(testdata);
							Arraydata.add(testdata);
						}
					}
				}
				break;

			}
		}
		WB.close();
		return Arraydata;
	}
}
