package Utility_Common_Methods;

import java.io.File;

public class Handle_Directory {
	public static File create_log_directory(String log_dir) {
		// Fetch the current Project Directory

		String project_dir = System.getProperty("user.dir"); // returns name of project where exec. happends (return
																// directory which we are working)
		System.out.println("Current project directory Path is : " + project_dir);
		File directory = new File(project_dir + "\\Api_logs\\" + log_dir);
		if (directory.exists()) {
			directory.delete();
			System.out.println(directory + " : deleted ");
			directory.mkdir();
			System.out.println(directory + " : Created ");
		} else {
			directory.mkdir();
			System.out.println(directory + " : Created ");
		}
		return directory;

	}
}