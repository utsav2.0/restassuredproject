package Testing_Pakage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_methods.common_method_handle_API;
import Endpoint.post_endpoint;
import Requeest_Repository.Post_request_repository;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;
import io.restassured.path.json.JsonPath;

public class Post_Test_Case extends common_method_handle_API {
@Test
	public static void executor() throws IOException {
		File log_dir=Handle_Directory.create_log_directory("Post_TestCase_logs");
		String requestbody = Post_request_repository.post_request_tc1();
		String endpoint = post_endpoint.post_endpoint_TC1();
	//	System.out.println("Post APi Triggered");
		for (int i = 0; i < 5; i++) {

			int StatusCode = post_statuscode(requestbody, endpoint);
			System.out.println(StatusCode);
			if (StatusCode == 201) {

				String responsebody = post_responseBody(requestbody, endpoint);
				System.out.println(responsebody);
				Handle_Api_logs.evidence_creator(log_dir,"Post_Test_Case", endpoint, requestbody, responsebody);
				Post_Test_Case.validator(requestbody, responsebody);
				break;
			} else {
				System.out.println("Expected status code is not found hence retrying ");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime curr_dt = LocalDateTime.now();
		String req_expdt = curr_dt.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_dt = jsp_res.getString("createdAt");
		res_dt = res_dt.substring(0, 11);

		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(req_expdt, res_dt);

	}
}
