package Testing_Pakage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_methods.common_method_handle_API;
import Endpoint.put_endpoint;
import Requeest_Repository.put_request_repository;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;
import io.restassured.path.json.JsonPath;

public class Put_Test_Case extends common_method_handle_API {
@Test
	public static void executor() throws IOException {
		File log_dir=Handle_Directory.create_log_directory("Put_TestCase_Logs");
		String requestbody = put_request_repository.put_repository_tc1();
		String endpoint = put_endpoint.put_endpoint_tc1();
	//	System.out.println("Put APi Triggered");
		for (int i = 0; i < 5; i++) {
			int statuscode = put_statuscode(requestbody, endpoint);
			System.out.println(statuscode);

			if (statuscode == 200) {
				String responsebody = put_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				Handle_Api_logs.evidence_creator(log_dir, "Put_Test_Case", endpoint, requestbody, responsebody);
				Put_Test_Case.validator(requestbody, responsebody);
				break;
			} else {
				System.out.println(" PUT Expected statuscode is not found hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedate = jsp_res.getString("updatedAt");
		res_updatedate = res_updatedate.substring(0, 11);

		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
		Assert.assertEquals(expecteddate, res_updatedate);
	}
}
