package Testing_Pakage;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_methods.common_method_handle_API;
import Endpoint.patch_endpoint;
import Requeest_Repository.Patch_request_repository;
import Utility_Common_Methods.Handle_Api_logs;
import Utility_Common_Methods.Handle_Directory;
import io.restassured.path.json.JsonPath;

public class Patch_Test_Case extends common_method_handle_API {
@Test
	public static void executor() throws IOException {
		File log_dir = Handle_Directory.create_log_directory("Patch_TestCase_logs");
		String requestbody = Patch_request_repository.patch_request_tc1();
		String endpoint = patch_endpoint.patch_endpont_TC1();
		System.out.println("Patch APi Triggered");
		for (int i = 0; i < 5; i++) {
			int statuscode = patch_statuscode(requestbody, endpoint);
			System.out.println(statuscode);

			if (statuscode == 200) {
				String responsebody = patch_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				Handle_Api_logs.evidence_creator(log_dir, "Post_Test_Case", endpoint, requestbody, responsebody);
				Patch_Test_Case.validator(requestbody, responsebody);
				break;
			} else {
				System.out.println("PATCH Expected status code not found hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);

	}

}
