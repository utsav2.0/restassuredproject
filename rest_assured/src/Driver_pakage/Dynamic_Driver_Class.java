package Driver_pakage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import Utility_Common_Methods.Excel_Data_Extractor;

public class Dynamic_Driver_Class {

	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> TC_Execute = Excel_Data_Extractor.Excel_data_reader("Test_data", "Test_Cases", "TC_Name");
		int count = TC_Execute.size();

		for (int i = 1; i < count; i++) {
			String TestCase_Name = TC_Execute.get(i);
			System.out.println(TestCase_Name);

			// Call the testcaseclass on runtime by using java.lang.reflect package

			Class<?> Test_class = Class.forName("Testing_Pakage." + TestCase_Name);

			// Call the execute method belonging to test class captured in variable
			// TestCase_Name by
			Method Execute_Method = Test_class.getDeclaredMethod("executor");
			// Set the accessibility of method true
			Execute_Method.setAccessible(true);

			// Create the instance of testclass captured in variable name testclassname

			Object instance_of_test_class = Test_class.getDeclaredConstructor().newInstance();

			// Execute the test script class fetched in variable Test_class
			Execute_Method.invoke(instance_of_test_class);

		}
	}
}