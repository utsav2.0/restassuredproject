package rest_assured;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class patch2 {

	public static void main(String[] args) {
		// Declare Baseurl
		RestAssured.baseURI = "https://reqres.in/";
		String requestbody = "{\r\n" + "    \"email\": \"eve.holt@reqres.in\",\r\n" + "    \"password\": \"pistol\"\r\n"
				+ "}";
		String responsebody = given().header("Content-Type", "application/json").body(requestbody).when().post("api/register").then()
				.extract().response().asString();
		System.out.println(responsebody);
		// Create an object of JSon Path to parse the requestbody
		JsonPath jsp_req = new JsonPath(requestbody);
	//	String req_email = jsp_req.getString("email");
	//	String req_pwd = jsp_req.getString("password");

		// create an object of JsonPath to parse ResponseBody
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_token= jsp_res.getString("token");
		 int res_id=jsp_res.getInt("id");

		// Validiting responsebody
		Assert.assertNotNull(res_id);
		Assert.assertNotNull(res_token);

	}

}
