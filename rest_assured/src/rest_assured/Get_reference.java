package rest_assured;

import static io.restassured.RestAssured.given;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import io.restassured.RestAssured;


public class Get_reference {

	public static void main(String[] args) {
		
		//Step 1 Storing expected values in varibales
		int excp_id[]= {1,2,3,4,5,6};
		String excp_email[]= {"george.bluth@reqres.in","janet.weaver@reqres.in","emma.wong@reqres.in","eve.holt@reqres.in","charles.morris@reqres.in","tracey.ramos@reqres.in"};
		String excp_fname[]= {"George","Janet","Emma","Eve","Charles","Tracey"};
		String excp_lname[]= {"Bluth","Weaver","Wong", "Holt","Morris","Ramos"};
	
		//Step 2 :- Declare baseurl
		
		RestAssured.baseURI = "https://reqres.in/";   
		
		// Step 3 Extract responsebody
		
		String responsebody = given().when().get("/api/users?page=1").then().extract().response().asString();
		System.out.println("responsebody is : " + responsebody);

		//Step4  Creating JSON object to parse resposnebody
		
		JSONObject res_array= new JSONObject(responsebody);
		JSONArray data_array=res_array.getJSONArray("data");
		int count=data_array.length();
		
		//Step 5 validating responsebody
		for (int i=0;i<count;i++)
		{
			int id=excp_id[i];
			String email=excp_email[i];
			String fname=excp_fname[i];
			String lname=excp_lname[i];
			
			int res_id=data_array.getJSONObject(i).getInt("id");
			String res_email=data_array.getJSONObject(i).getString("email");
			String res_fname=data_array.getJSONObject(i).getString("first_name");
			String res_lname=data_array.getJSONObject(i).getString("last_name");
			
			Assert.assertEquals(res_id,id);
			Assert.assertEquals(res_email,email);
			Assert.assertEquals(res_fname,fname);
			Assert.assertEquals(res_lname,lname);
			
		}
		
		

	}

}
