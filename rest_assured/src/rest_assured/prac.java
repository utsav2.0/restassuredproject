package rest_assured;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class prac {

	public static void main(String[] args) {
		// Configuring bse url
		RestAssured.baseURI = "https://reqres.in";

		// configuring resquest and responsebody
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users").then()
				.extract().response().asString();
		System.out.println("responsebody is " +responseBody);
		// exacting Jsopn path
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime Curr_dt = LocalDateTime.now();
		String exp_dt = Curr_dt.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_createdat = jsp_res.getString("createdAt");
		res_createdat=res_createdat.toString().substring(0,11);

		// Validation

		Assert.assertEquals(req_name, res_name);
		Assert.assertEquals(req_job, res_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(exp_dt, res_createdat);

	}

}
